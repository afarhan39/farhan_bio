import React, { Component } from 'react'
import logo404 from '../../img/404.svg'

class NotFound extends Component {
  render() {
    return (
      <div className="page-content">
        <div className="flex flex-wrap">
          <div className="flex-item" />
          <div className="flex-item" />
          <div className="flex-item flex-column flex">
            <div className="flex-item" />
            <div className="flex-item" />
          </div>
          <div className="flex-item" />
          <div className="flex-item" />
          <div className="flex-item" />
        </div>
      </div>
    )
  }
}

export default NotFound
