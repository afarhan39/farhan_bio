import React, { Component } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import NotFound from './app/notfound/NotFound'
import Blog from './app/blog/Blog'
import Home from './app/home/Home'
import App from './app/App'

class Router extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path={'/'} component={Home} />
          <Route exact path={'/app'} component={App} />
          <Route exact path={'/blog'} component={Blog} />
          <Route component={NotFound} />
        </Switch>
      </BrowserRouter>
    )
  }
}

export default Router
